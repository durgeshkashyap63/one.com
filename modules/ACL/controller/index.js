exports.getProduct = getProduct;
exports.addProduct = addProduct;
exports.updateProduct = updateProduct;
exports.deleteProduct = deleteProduct;

function getProduct(req, res) {
    return res.send({ message: "Products sent successfully", status: 200 })
}

function addProduct(req, res) {
    return res.send({ message: "Products addded successfully", status: 201 })
}

function updateProduct(req, res) {
    return res.send({ message: "Products updated successfully", status: 200 })
}

function deleteProduct(req, res) {
    return res.send({ message: "Products deleted successfully", status: 200 })
}