const express = require('express');
const bodyParser= require('body-parser');
const PORT = 3000;
const app = express();
app.use(bodyParser.urlencoded({extended:true}));


global.app = app;
require('./middleware')
require('./modules')

app.listen(PORT,()=>{
    console.log("Express running at ",PORT)
})
