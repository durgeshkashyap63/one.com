const mongoose = require('mongoose');

const schemaName = "signup";
const schema = new mongoose.Schema({
    username:{
        type: String
    },
    password:{
        type: String
    },
    role:{
        type: String
    }
})

const signupSchema = mongoose.model(schemaName,schema)

module.exports =signupSchema;