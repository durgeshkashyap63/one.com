const jwt = require('jsonwebtoken')

const JWT_KEY = "one.com"

exports.checkRole = checkRole;

//GET -> Fetch
//POST -> Create
//PATCH/PUT -> Update
//DELETE -> Delete

let userRoleMethod = {
    admin: ["GET", "POST", "PATCH", "PUT", "DELETE"],
    seller: ["GET", "POST", "PATCH", "PUT"],
    supporter: ["GET", "DELETE"],
    customer: ["GET"]
}

function checkRole(req, res, next) {
    let accesstoken = req.headers.accesstoken;
    let data = jwt.verify(accesstoken, JWT_KEY)
    if (userRoleMethod[data.role].includes(req.method)) {
        return next();
    }
    return res.send({ "message": "“Not authorized to access endpoint”", status: 401 })
}