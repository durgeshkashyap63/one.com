const mongoose = require('mongoose');

exports.insertData = insertData;
exports.getDataArray = getDataArray;
exports.updateUpsert = updateUpsert;

function insertData(tablename, obj) {
    return new Promise(async (resolve, reject) => {
        let model = await mongoose.model(tablename);
        let collection = new model(obj);
        let response = await collection.save({ lean: true });
        resolve(response)
    })
}

function getDataArray(tablename, fieldName,value) {
    return new Promise(async (resolve, reject) => {
        let obj ={}
        let index= 0;
        for(let el of fieldName){
            obj[el]=value[index];
            index++;
        }
        let response = await mongoose.model(tablename).find(obj).lean().exec();
        resolve(response)
    })
}


function updateUpsert(tablename,where, value) {
    return new Promise(async (resolve, reject) => {
        let response = await mongoose.model(tablename).findOneAndUpdate(where,value,{ upsert: true,new:true });
        resolve(response)
    })
}
