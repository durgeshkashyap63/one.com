const Joi = require('joi');
const validate =require('./../../../validation')

exports.signUp = signUp;
exports.login =login;

function signUp(req,res,next){
    let schema = Joi.object().keys({
        username:Joi.string().required(),
        password: Joi.string().required(),
        role: Joi.string().valid('admin', 'seller', 'supporter', 'customer').required()
    })
    let checkValid = validate.validateFields(req.body,res,schema)
    console.log(checkValid)
    if(checkValid){
        next();
    }
}


function login(req,res,next){
    let schema = Joi.object().keys({
        username:Joi.string().required(),
        password: Joi.string().required(),
    })
    let checkValid = validate.validateFields(req.body,res,schema)
    console.log(checkValid)
    if(checkValid){
        next();
    }
}