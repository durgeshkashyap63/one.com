const db = require('./../commonService/db')
const jwt = require('jsonwebtoken')

exports.validateAccessToken = validateAccessToken;

async function validateAccessToken(req, res,next) {
    let accesstoken = req.headers.accesstoken;
    let checkExist  = await db.getDataArray("token",["token"],[accesstoken])
    if(!checkExist.length){
    return res.send({ "message": "“Invalid token”", status: 401 })
    }
    let data = jwt.verify(accesstoken, "one.com")
    if (data) {
        return next()
    }
    return res.send({ "message": "“Token Expired”", status: 401 })
}