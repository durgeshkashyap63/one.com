const ACLMiddleware = require('./../../middleware/aclMiddleware')
const accessTokenValidation = require('./../../middleware/validateAccessToken')
const ACLController = require('./controller')

app.get('/products',accessTokenValidation.validateAccessToken, ACLMiddleware.checkRole,ACLController.getProduct)
//RESPONSE for admin: {
//     "message": "Products sent successfully",
//     "status": 200
// }
app.post('/products',accessTokenValidation.validateAccessToken, ACLMiddleware.checkRole,ACLController.addProduct)
//RESPONSE for admin: {
//     "message": "Products sent successfully",
//     "status": 200
// }
app.put('/products',accessTokenValidation.validateAccessToken, ACLMiddleware.checkRole,ACLController.updateProduct)
//RESPONSE for admin: {
//     "message": "Products sent successfully",
//     "status": 200
// }
app.patch('/products',accessTokenValidation.validateAccessToken, ACLMiddleware.checkRole,ACLController.updateProduct)
//RESPONSE for admin: {
//     "message": "Products sent successfully",
//     "status": 200
// }
app.delete('/products',accessTokenValidation.validateAccessToken,  ACLMiddleware.checkRole,ACLController.deleteProduct)
//RESPONSE for admin: {
//     "message": "Products sent successfully",
//     "status": 200
// }