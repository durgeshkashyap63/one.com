const mongoose = require('mongoose');

const schemaName = "token";
const schema = new mongoose.Schema({
    user_id:{
        type: String
    },
    token:{
        type: String
    }
})

const signupSchema = mongoose.model(schemaName,schema)

module.exports =signupSchema;