
const db = require('./../../../commonService/db')
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken")

exports.signUp = signUp;
exports.login = login;

const JWT_KEY = "one.com"

async function signUp(req, res) {
    let obj = req.body;
    obj.password = bcrypt.hashSync(req.body.password, 8)
    let checkExist = await db.getDataArray("signup", ["username"], [obj.username])
    if (checkExist.length) {
        return res.send({ "message": "Username already exist", "status": 400 })
    }
    await db.insertData("signup", obj)
    return res.send({ "message": "User Registered", "status": 200 })
}

async function login(req, res) {
    let obj = req.body;
    let checkExist = await db.getDataArray("signup", ["username"], [obj.username])
    if (checkExist.length) {
        let passwordCheck = bcrypt.compareSync(req.body.password, checkExist[0].password)
        if (!passwordCheck) {
            return res.send({ "message": "Password Invalid", "status": 200 })
        }
        obj.role = checkExist[0].role;
        let accessToken = jwt.sign(obj, JWT_KEY, { expiresIn: "1000h" });
        await db.updateUpsert("token", {user_id:checkExist[0]._id},{$set:{token:accessToken}});
        return res.send({ "message": "LoggedIn Successfully", "status": 200, data: { accessToken: accessToken } })
    } else {
        return res.send({ "message": "Username does not exist", "status": 400 })
    }
}