const authenticationValidator = require('./validator')
const authenticationController = require('./controller')

app.post('/signup',authenticationValidator.signUp,authenticationController.signUp)
//Parameter: role
//RESPONSE: {
//     "message": "User Registered",
//     "status": 200
// }


app.post('/login',authenticationValidator.login,authenticationController.login)
//Parameter: username,password
//RESPONSE: {
//     "message": "LoggedIn Successfully",
//     "status": 200,
//     "data": {
//         "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImR1cmdlc2gxIiwicGFzc3dvcmQiOiJmc2Rmc2QiLCJyb2xlIjoiY3VzdG9tZXIiLCJpYXQiOjE2NjI1NDAyNzcsImV4cCI6MTY2NjE0MDI3N30.6vbO3gzetegKvsngeDUpxrGIpkQxpgSQT4R-Od3NMUs"
//     }
// }