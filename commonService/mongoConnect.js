const mongoose = require('mongoose');

let MONGO_URL = 'mongodb://127.0.0.1:27017/OneDotCom'

mongoose.connect(MONGO_URL).then(() => {
    console.log("Mongo Connected");
}).catch(err => {
    console.log(err)
    console.log("Error! Mongo not able to connect",err.message)
})