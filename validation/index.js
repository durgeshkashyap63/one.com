
const Joi = require('joi');

exports.validateFields =validateFields;

function validateFields(req,res,schema){
    let validateFields = schema.validate(req);
    if(validateFields.error){
        let errorDetail = validateFields.error.details?validateFields.error.details[0].message:"Parameter Missing";
        res.send({message:errorDetail,status:400});
        return false;
    }
    return true;
}

